require 'spec_helper'

describe JobApplication do
  let(:job_offer) { JobOffer.new(title: 'a title') }

  describe 'valid?' do
    it 'should be invalid when email is blank' do
      check_validation(:applicant_email, "Applicant email can't be blank") do
        described_class.create_for(nil, job_offer, 'https://www.linkedin.com/test', 1)
      end
    end

    it 'should be invalid when offer is blank' do
      check_validation(:job_offer, "Job offer can't be blank") do
        described_class.create_for('applicant@test.com', nil, 'https://www.linkedin.com/test', 1)
      end
    end

    it 'should be invalid when link to cv is blank' do
      check_validation(:link_to_cv, 'Link to cv The link to your CV is required') do
        described_class.create_for('applicant@test.com', job_offer, nil, 1)
      end
    end

    it 'should be invalid when link to cv is not in URL format' do
      check_validation(:link_to_cv, 'Link to cv The link to your CV is invalid') do
        described_class.create_for('applicant@test.com', job_offer, 'test @ abcd', 1)
      end
    end

    it 'should be invalid when user_id is nil' do
      check_validation(:user_id, 'User is not a number') do
        described_class.create_for('applicant@test.com', job_offer, 'https://www.linkedin.com/test', nil)
      end
    end

    it 'should be invalid when user_id is an empty string' do
      check_validation(:user_id, 'User is not a number') do
        described_class.create_for('applicant@test.com', job_offer, 'https://www.linkedin.com/test', '')
      end
    end

    it 'should be invalid when user_id is negative' do
      check_validation(:user_id, 'User must be greater than or equal to 0') do
        described_class.create_for('applicant@test.com', job_offer, 'https://www.linkedin.com/test', -1)
      end
    end
  end

  describe 'create_for' do
    it 'should set applicant_email' do
      email = 'applicant@test.com'
      ja = described_class.create_for(email, job_offer, 'https://www.linkedin.com/test', 1)
      expect(ja.applicant_email).to eq(email)
    end

    it 'should set job_offer' do
      offer = job_offer
      ja = described_class.create_for('applicant@test.com', offer, 'https://www.linkedin.com/test', 1)
      expect(ja.job_offer).to eq(offer)
    end

    it 'should set link_to_cv' do
      link_to_cv = 'https://www.linkedin.com/test'
      ja = described_class.create_for('applicant@test.com', job_offer, link_to_cv, 1)
      expect(ja.link_to_cv).to eq(link_to_cv)
    end

    it 'should set user_id' do
      user_id = 1
      ja = described_class.create_for('applicant@test.com', job_offer, 'https://www.linkedin.com/test', user_id)
      expect(ja.user_id).to eq(user_id)
    end
  end

  describe 'send_contact_email_to_applicant' do
    it 'should deliver contact info notification' do
      ja = described_class.create_for('applicant@test.com', job_offer, 'https://www.linkedin.com/test', 1)
      expect(JobVacancy::App).to receive(:deliver).with(:notification, :contact_info_email, ja)
      ja.send_contact_email_to_applicant
    end
  end

  describe 'send_applicant_info_to_offerer' do
    it 'should deliver applicant info notification' do
      job_offer.owner = User.new(id: 1, name: 'Juan', email: 'test123@test.com')
      ja = described_class.create_for('applicant@test.com', job_offer, 'https://www.linkedin.com/test', 1)
      expect(JobVacancy::App).to receive(:deliver).with(:notification, :applicant_info_email, ja)
      ja.send_applicant_info_to_offerer
    end
  end
end
