require 'spec_helper'

describe JobOffer do
  describe 'valid?' do
    it 'should be invalid when title is blank' do
      check_validation(:title, "Title can't be blank") do
        described_class.new(location: 'a location')
      end
    end

    it 'should be valid when title is not blank' do
      job_offer = described_class.new(title: 'a title')
      expect(job_offer).to be_valid
    end

    it 'should be invalid when salary is less than 0' do
      check_validation(:salary, 'Salary Remuneration should be a positive number') do
        described_class.new(title: 'a title', salary: -5)
      end
    end

    it 'should be valid when salary is equal to 0' do
      job_offer = described_class.new(title: 'a title', salary: 0)
      expect(job_offer).to be_valid
    end

    it 'should be valid when salary is greater than 0' do
      job_offer = described_class.new(title: 'a title', salary: 1000)
      expect(job_offer).to be_valid
    end

    it 'should be invalid when the offer has more than 5 tags' do
      check_validation(:tags, 'Tags The offer cannot have more than five tags') do
        described_class.new(title: 'a title', raw_tags: 'java c# c++ pascal ruby cobol')
      end
    end
  end
end
