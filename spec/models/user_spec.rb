require 'spec_helper'

describe User do
  subject(:user) { described_class.new({}) }

  describe 'model' do
    it { is_expected.to respond_to(:id) }
    it { is_expected.to respond_to(:name) }
    it { is_expected.to respond_to(:crypted_password) }
    it { is_expected.to respond_to(:email) }
    it { is_expected.to respond_to(:job_offers) }
  end

  describe 'valid?' do
    it 'should be false when name is blank' do
      user = described_class.new(email: 'john.doe@someplace.com',
                                 crypted_password: 'a_secure_passWord!')
      expect(user.valid?).to eq false
      expect(user.errors).to have_key(:name)
    end

    it 'should be false when email is not valid' do
      user = described_class.new(name: 'John Doe', email: 'john',
                                 crypted_password: 'a_secure_passWord!')
      expect(user.valid?).to eq false
      expect(user.errors).to have_key(:email)
    end

    it 'should be false when password is blank' do
      user = described_class.new(name: 'John Doe', email: 'john')
      expect(user.valid?).to eq false
      expect(user.errors).to have_key(:crypted_password)
    end

    it 'should be true when all field are valid' do
      user = described_class.new(name: 'John Doe', email: 'john@doe.com',
                                 crypted_password: 'a_secure_passWord!')
      expect(user.valid?).to eq true
    end
  end

  describe 'has password?' do
    let(:password) { 'password' }
    let(:user) do
      described_class.new(password: password,
                          email: 'john.doe@someplace.com',
                          name: 'john doe')
    end

    it 'should return false when password do not match' do
      expect(user).not_to have_password('invalid')
    end

    it 'should return true when password do  match' do
      expect(user).to have_password(password)
    end
  end

  describe 'is valid password?' do
    it 'should be true when password is strong' do
      user = described_class.new(password: 'u2345M7890',
                                 email: 'john.doe@someplace.com',
                                 name: 'john doe')
      expect(user.is_valid_password?).to eq(true)
    end

    it 'should be false when password is weak' do
      user = described_class.new(password: 'u2345M789',
                                 email: 'john.doe@someplace.com',
                                 name: 'john doe')
      expect(user.is_valid_password?).to eq(false)
    end
  end

  describe 'generate password recovery token' do
    it 'it should generate a password restore token with an expiration' do
      user = described_class.new
      user.generate_password_recovery_token
      expect(user.password_recovery_token).not_to be(nil)
      expect(user.password_recovery_token_expires_at).not_to be(nil)
    end
  end

  describe 'is password recovery token expired' do
    it 'it should return false if token is not expired' do
      user = described_class.new
      user.password_recovery_token_expires_at = Time.now + 1.hour
      expect(user.is_password_recovery_token_expired?).to eq(false)
    end

    it 'it should return true if token is expired' do
      user = described_class.new
      user.password_recovery_token_expires_at = Time.now - 1.minute
      expect(user.is_password_recovery_token_expired?).to eq(true)
    end
  end

  describe 'change_password' do
    it 'it should change the user password' do
      user = described_class.new(password: 'u2345M7890',
                                 email: 'john.doe@someplace.com',
                                 name: 'john doe')
      user.change_password('12345678Er')
      expect(user.password).to eq('12345678Er')
    end
  end
end
