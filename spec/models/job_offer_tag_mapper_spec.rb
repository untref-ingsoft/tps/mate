require 'spec_helper'

describe JobOfferTagMapper do
  describe 'from_string' do
    it 'should return one job_offer_tag if one tag is passed' do
      job_offer_tag_mapper = described_class.new
      job_offer_tags = job_offer_tag_mapper.from_string(1, 'java')

      expect(job_offer_tags[0].job_offer_id).to eq(1)
      expect(job_offer_tags[0].tag).to eq('java')
    end

    it 'should return an array of job_offer_tag if multiple tags are passed' do
      job_offer_tag_mapper = described_class.new
      job_offer_tags = job_offer_tag_mapper.from_string(1, 'java c#')

      expect(job_offer_tags.length).to eq(2)
      expect(job_offer_tags[0].tag).to eq('java')
      expect(job_offer_tags[1].tag).to eq('c#')
    end

    it 'should return an empty array of job_offer_tag if no tags are passed' do
      job_offer_tag_mapper = described_class.new
      job_offer_tags = job_offer_tag_mapper.from_string(1, nil)
      expect(job_offer_tags.length).to eq(0)
    end

    it 'should delete duplicates' do
      job_offer_tag_mapper = described_class.new
      job_offer_tags = job_offer_tag_mapper.from_string(1, 'Java jAva jaVa javA JAva jAVa')
      expect(job_offer_tags.length).to eq(1)
    end
  end

  describe 'to_string' do
    it 'should return a string with whitespace delimiters' do
      job_offer_tag_one = JobOfferTag.new(id: 1, job_offer_id: 1, tag: 'java')
      job_offer_tag_two = JobOfferTag.new(id: 2, job_offer_id: 1, tag: 'c#')

      job_offer_tag_mapper = described_class.new
      raw_tags = job_offer_tag_mapper.to_string([job_offer_tag_one, job_offer_tag_two])
      expect(raw_tags).to eq('java c#')
    end

    it 'should return an empty string if no tags are passed' do
      job_offer_tag_mapper = described_class.new
      raw_tags = job_offer_tag_mapper.to_string(nil)
      expect(raw_tags).to eq('')
    end
  end
end
