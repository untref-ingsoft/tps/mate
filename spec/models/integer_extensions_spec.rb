require 'spec_helper'

describe IntegerExtensions do
  describe 'number_or_zero' do
    it 'should return 0 when parameter is nil' do
      result = described_class.new.number_or_zero(nil)
      expect(result).to eq(0)
    end

    it 'should return 0 when parameter is not an integer' do
      result = described_class.new.number_or_zero('NOT_AN_INTEGER')
      expect(result).to eq(0)
    end

    it 'should return an intenger when parameter is an integer in string format' do
      result = described_class.new.number_or_zero('9')
      expect(result).to eq(9)
    end

    it 'should return an intenger when parameter is an integer' do
      result = described_class.new.number_or_zero(3)
      expect(result).to eq(3)
    end
  end
end
