require 'spec_helper'

describe JobOfferTag do
  describe 'model' do
    it { is_expected.to respond_to(:id) }
    it { is_expected.to respond_to(:job_offer_id) }
    it { is_expected.to respond_to(:tag) }
  end
end
