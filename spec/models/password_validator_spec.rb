require 'spec_helper'

describe PasswordValidator do
  describe 'validate?' do
    it 'should be true when password is strong' do
      password_validator = described_class.new('u2345M7890')
      result = password_validator.validate?
      expect(result).to be(true)
    end

    it 'should be false when password has 9 characters' do
      password_validator = described_class.new('u2345M789')
      result = password_validator.validate?
      expect(result).to be(false)
    end

    it 'should be false when password does not have an uppercase letter' do
      password_validator = described_class.new('u234567890')
      result = password_validator.validate?
      expect(result).to be(false)
    end

    it 'should be false when password does not have a lowercase letter' do
      password_validator = described_class.new('T234567890')
      result = password_validator.validate?
      expect(result).to be(false)
    end

    it 'should be false when password does not have a digit' do
      password_validator = described_class.new('AbCDEfGhiZ')
      result = password_validator.validate?
      expect(result).to be(false)
    end
  end
end
