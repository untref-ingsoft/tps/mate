require 'spec_helper'

describe RecoveryPasswordEmailSender do
  describe 'send_for' do
    let!(:existing_user) do
      user = User.new(name: 'Joe', email: 'juangomez@gmail.com', crypted_password: 'Asdasd1234')
      UserRepository.new.save(user)
      user
    end

    it 'should not send password recovery email when user does not exists in database' do
      described_class.new.send_for('doesnotexist@gmail.com', '')
      file_exists = File.exist?("#{Padrino.root}/tmp/emails/doesnotexist@gmail.com")
      expect(file_exists).to eq(false)
    end

    it 'should send password recovery email when user exists in database' do
      described_class.new.send_for(existing_user.email, '')
      file = File.open("#{Padrino.root}/tmp/emails/#{existing_user.email}", 'r')
      content = file.read
      content.include?("We've recieved a request to reset your password").should be true
    end
  end
end
