require 'spec_helper'

describe ChangePasswordManager do
  let!(:existing_user) do
    user = User.new(name: 'Joe', email: 'juangomez@gmail.com', password: 'Asdasd1234')
    user.generate_password_recovery_token
    UserRepository.new.save(user)
    user
  end

  describe 'change_user_password' do
    it 'should raise error when password do not match with password_confirmation' do
      message = 'Passwords do not match'
      expect do
        described_class.new.change_user_password(existing_user.password_recovery_token, 'u2345M7890',
                                                 'u2345M7891')
      end.to raise_error(ChangePasswordError, message)
    end

    it 'should raise error when password is incorrectly' do
      message = 'Invalid password: it must contain an uppercase, a lowercase, a number and 10 characters length'
      expect do
        described_class.new.change_user_password(existing_user.password_recovery_token, 'u2345M', 'u2345M')
      end.to raise_error(ChangePasswordError, message)
    end
  end
end
