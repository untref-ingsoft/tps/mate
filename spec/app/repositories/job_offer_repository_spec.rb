require 'integration_spec_helper'

describe JobOfferRepository do
  let(:repository) { described_class.new }

  let(:owner) do
    user = User.new(name: 'Joe', email: 'joe@doe.com', crypted_password: 'secure_pwd')
    UserRepository.new.save(user)
    user
  end

  describe 'deactive_old_offers' do
    let!(:today_offer) do
      today_offer = JobOffer.new(title: 'a title',
                                 updated_on: Date.today,
                                 is_active: true,
                                 user_id: owner.id)
      repository.save(today_offer)
      today_offer
    end

    let!(:thirty_day_offer) do
      thirty_day_offer = JobOffer.new(title: 'a title',
                                      updated_on: Date.today - 45,
                                      is_active: true,
                                      user_id: owner.id)
      repository.save(thirty_day_offer)
      thirty_day_offer
    end

    it 'should deactivate offers updated 45 days ago' do
      repository.deactivate_old_offers

      updated_offer = repository.find(thirty_day_offer.id)
      expect(updated_offer.is_active).to eq false
    end

    it 'should not deactivate offers created today' do
      repository.deactivate_old_offers

      not_updated_offer = repository.find(today_offer.id)
      expect(not_updated_offer.is_active).to eq true
    end
  end

  describe 'get_suggestions' do
    let!(:java_offer) do
      java_offer = JobOffer.new(title: 'Java jr',
                                updated_on: Date.today,
                                is_active: true,
                                user_id: owner.id)
      described_class.new.save(java_offer)

      job_offer_tag_one = JobOfferTag.new(job_offer_id: java_offer.id, tag: 'java')

      JobOfferTagRepository.new.save(job_offer_tag_one)

      java_offer
    end

    let!(:ruby_sr_job_offer) do
      ruby_job_offer = JobOffer.new(title: 'Ruby Developer SR',
                                    updated_on: Date.today,
                                    is_active: true,
                                    user_id: owner.id)
      described_class.new.save(ruby_job_offer)

      job_offer_tag_one = JobOfferTag.new(job_offer_id: ruby_job_offer.id, tag: 'ruby')
      JobOfferTagRepository.new.save(job_offer_tag_one)

      job_offer_tag_two = JobOfferTag.new(job_offer_id: ruby_job_offer.id, tag: 'ruby-sr')
      JobOfferTagRepository.new.save(job_offer_tag_two)

      job_offer_tag_three = JobOfferTag.new(job_offer_id: ruby_job_offer.id, tag: 'ruby-on-rails')
      JobOfferTagRepository.new.save(job_offer_tag_three)

      ruby_job_offer
    end

    let!(:ruby_jr_job_offer) do
      ruby_job_offer = JobOffer.new(title: 'Ruby Developer JR',
                                    updated_on: Date.today,
                                    is_active: true,
                                    user_id: owner.id)
      described_class.new.save(ruby_job_offer)

      job_offer_tag_one = JobOfferTag.new(job_offer_id: ruby_job_offer.id, tag: 'ruby')
      JobOfferTagRepository.new.save(job_offer_tag_one)

      job_offer_tag_two = JobOfferTag.new(job_offer_id: ruby_job_offer.id, tag: 'ruby-jr')
      JobOfferTagRepository.new.save(job_offer_tag_two)

      job_offer_tag_three = JobOfferTag.new(job_offer_id: ruby_job_offer.id, tag: 'ruby-on-rails')
      JobOfferTagRepository.new.save(job_offer_tag_three)

      ruby_job_offer
    end

    it 'should not suggest itself' do
      job_offer_suggestion = repository.get_suggestions(java_offer.id, 2)
      expect(job_offer_suggestion.length).to eq(0)
    end

    it 'should suggest another job offer and not itself' do
      job_offer_suggestion = repository.get_suggestions(ruby_sr_job_offer.id, 2)
      expect(job_offer_suggestion.length).to eq(1)
      expect(job_offer_suggestion[0].id).to eq(ruby_jr_job_offer.id)
    end
  end

  describe 'full_search' do
    let!(:net_developer) do
      offer_one = JobOffer.new(title: 'NET Developer',
                               created_on: Date.today,
                               is_active: true,
                               user_id: owner.id)
      described_class.new.save(offer_one)
      offer_one
    end

    let!(:net_developer_with_description) do
      offer_two = JobOffer.new(title: 'an offer',
                               description: 'Required: Entity Framework, NET framework and ASP.NET Core',
                               created_on: Date.today,
                               is_active: true,
                               user_id: owner.id)
      described_class.new.save(offer_two)
      offer_two
    end

    let!(:net_developer_with_location) do
      offer_three = JobOffer.new(title: 'an offer',
                                 location: 'The NET Company',
                                 created_on: Date.today,
                                 is_active: true,
                                 user_id: owner.id)
      described_class.new.save(offer_three)
      offer_three
    end

    it 'should search by case insensitive' do
      offer_matches = repository.full_search('net')
      match = offer_matches.select { |offer| offer.title == net_developer.title }
      expect(match.length).to eq(1)
    end

    it 'should search by description' do
      offer_matches = repository.full_search('net')
      match = offer_matches.select { |offer| offer.description == net_developer_with_description.description }
      expect(match.length).to eq(1)
    end

    it 'should search by location' do
      offer_matches = repository.full_search('net')
      match = offer_matches.select { |offer| offer.location == net_developer_with_location.location }
      expect(match.length).to eq(1)
    end
  end

  describe 'full_search by tags' do
    let!(:net_developer_with_tags) do
      offer_four = JobOffer.new(title: 'an offer',
                                created_on: Date.today,
                                is_active: true,
                                user_id: owner.id)
      described_class.new.save(offer_four)

      offer_four_tag_one = JobOfferTag.new(job_offer_id: offer_four.id, tag: 'net')
      JobOfferTagRepository.new.save(offer_four_tag_one)

      offer_four_tag_two = JobOfferTag.new(job_offer_id: offer_four.id, tag: 'net-core')
      JobOfferTagRepository.new.save(offer_four_tag_two)

      offer_four
    end

    it 'should search by tags' do
      offer_matches = repository.full_search('net')
      match = offer_matches.select { |offer| offer.id == net_developer_with_tags.id }
      expect(match.length).to eq(1)
    end
  end

  describe 'increment_applicants' do
    let!(:offer) do
      offer = JobOffer.new(title: 'an offer',
                           created_on: Date.today,
                           is_active: true,
                           user_id: owner.id)
      described_class.new.save(offer)

      offer
    end

    let!(:offer_with_applicants) do
      offer_with_applicants = JobOffer.new(title: 'an offer with applicants',
                                           created_on: Date.today,
                                           is_active: true,
                                           user_id: owner.id,
                                           number_of_applicants: 5)
      described_class.new.save(offer_with_applicants)

      offer_with_applicants
    end

    it 'should increment number of applicants of an offer without applicants' do
      repository.increment_applicants(offer.id)
      updated_offer = repository.find(offer.id)
      expect(updated_offer.number_of_applicants).to eq(1)
    end

    it 'should increment number of applicants of an offer with applicants' do
      repository.increment_applicants(offer_with_applicants.id)
      updated_offer = repository.find(offer_with_applicants.id)
      expect(updated_offer.number_of_applicants).to eq(6)
    end
  end
end
