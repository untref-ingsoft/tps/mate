require 'integration_spec_helper'

describe JobOfferTagRepository do
  let(:repository) { described_class.new }

  let(:owner) do
    user = User.new(name: 'Joe', email: 'joe@doe.com', crypted_password: 'secure_pwd')
    UserRepository.new.save(user)
    user
  end

  describe 'save' do
    let!(:job_offer) do
      job_offer = JobOffer.new(title: 'a title',
                               updated_on: Date.today,
                               is_active: true,
                               user_id: owner.id)
      JobOfferRepository.new.save(job_offer)
      job_offer
    end

    it 'should normalize the tag name' do
      job_offer_tag = JobOfferTag.new(job_offer_id: job_offer.id,
                                      tag: 'JAVA')

      repository.save(job_offer_tag)
      tag_offer = repository.find(job_offer_tag.id)
      expect(tag_offer.tag).to eq 'java'
    end
  end

  describe 'delete_by_job_offer_id' do
    let!(:job_offer) do
      job_offer = JobOffer.new(title: 'a title',
                               updated_on: Date.today,
                               is_active: true,
                               user_id: owner.id)
      JobOfferRepository.new.save(job_offer)

      job_offer_tag_one = JobOfferTag.new(job_offer_id: job_offer.id, tag: 'java')
      described_class.new.save(job_offer_tag_one)

      job_offer_tag_two = JobOfferTag.new(job_offer_id: job_offer.id, tag: 'ruby')
      described_class.new.save(job_offer_tag_two)

      job_offer
    end

    let!(:another_job_offer) do
      another_job_offer = JobOffer.new(title: 'another title',
                                       updated_on: Date.today,
                                       is_active: true,
                                       user_id: owner.id)
      JobOfferRepository.new.save(another_job_offer)

      job_offer_tag_two = JobOfferTag.new(job_offer_id: another_job_offer.id, tag: 'ruby')
      described_class.new.save(job_offer_tag_two)

      another_job_offer
    end

    it 'should delete all existing tags' do
      repository.delete_by_job_offer_id(job_offer.id)
      job_offer_tags_deleted = repository.find_by_job_offer_id(job_offer.id)
      job_offer_tags_not_deleted = repository.find_by_job_offer_id(another_job_offer.id)
      expect(job_offer_tags_deleted.length).to eq(0)
      expect(job_offer_tags_not_deleted[0].job_offer_id).to eq(another_job_offer.id)
    end
  end

  describe 'get_related_tags_by_offer_id' do
    let!(:java_offer) do
      java_offer = JobOffer.new(title: 'Java jr',
                                updated_on: Date.today,
                                is_active: true,
                                user_id: owner.id)
      JobOfferRepository.new.save(java_offer)

      job_offer_tag_one = JobOfferTag.new(job_offer_id: java_offer.id, tag: 'java')
      described_class.new.save(job_offer_tag_one)

      java_offer
    end

    let!(:ruby_sr_job_offer) do
      ruby_job_offer = JobOffer.new(title: 'Ruby Developer SR',
                                    updated_on: Date.today,
                                    is_active: true,
                                    user_id: owner.id)
      JobOfferRepository.new.save(ruby_job_offer)

      job_offer_tag_one = JobOfferTag.new(job_offer_id: ruby_job_offer.id, tag: 'ruby')
      described_class.new.save(job_offer_tag_one)

      job_offer_tag_two = JobOfferTag.new(job_offer_id: ruby_job_offer.id, tag: 'ruby-sr')
      described_class.new.save(job_offer_tag_two)

      job_offer_tag_three = JobOfferTag.new(job_offer_id: ruby_job_offer.id, tag: 'ruby-on-rails')
      described_class.new.save(job_offer_tag_three)

      ruby_job_offer
    end

    let!(:ruby_jr_job_offer) do
      ruby_job_offer = JobOffer.new(title: 'Ruby Developer JR',
                                    updated_on: Date.today,
                                    is_active: true,
                                    user_id: owner.id)
      JobOfferRepository.new.save(ruby_job_offer)

      job_offer_tag_one = JobOfferTag.new(job_offer_id: ruby_job_offer.id, tag: 'ruby')
      described_class.new.save(job_offer_tag_one)

      job_offer_tag_two = JobOfferTag.new(job_offer_id: ruby_job_offer.id, tag: 'ruby-jr')
      described_class.new.save(job_offer_tag_two)

      job_offer_tag_three = JobOfferTag.new(job_offer_id: ruby_job_offer.id, tag: 'ruby-on-rails')
      described_class.new.save(job_offer_tag_three)

      ruby_job_offer
    end

    it 'should not suggest itself' do
      job_offer_tags_suggestion = repository.get_related_tags_by_offer_id(java_offer.id)
      expect(job_offer_tags_suggestion.length).to eq(0)
    end

    it 'should suggest another job offer and not itself' do
      job_offer_tags_suggestion = repository.get_related_tags_by_offer_id(ruby_sr_job_offer.id)
      expect(job_offer_tags_suggestion.length).to eq(2)
      expect(job_offer_tags_suggestion[0].job_offer_id).to eq(ruby_jr_job_offer.id)
    end
  end
end
