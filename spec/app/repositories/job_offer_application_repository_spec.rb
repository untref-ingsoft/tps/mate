require 'integration_spec_helper'

describe JobOfferApplicationRepository do
  let(:repository) { described_class.new }

  let(:owner) do
    user = User.new(name: 'Joe', email: 'joe@doe.com', crypted_password: 'secure_pwd')
    UserRepository.new.save(user)
    user
  end

  let(:applicant_user) do
    user = User.new(name: 'John', email: 'john@doe.com', crypted_password: 'secure_pwd')
    UserRepository.new.save(user)
    user
  end

  let!(:job_offer) do
    job_offer = JobOffer.new(title: 'a title', user_id: owner.id)
    JobOfferRepository.new.save(job_offer)
    job_offer
  end

  let!(:job_application) do
    job_application = JobApplication.create_for(
      applicant_user.email,
      job_offer,
      'https://www.linkedin.com/in/juan-gomez/',
      applicant_user.id
    )
    job_application
  end

  describe 'save' do
    it 'should save applicant_email' do
      repository.save(job_application)
      ja = repository.find(job_application.id)
      expect(ja.applicant_email).to eq(applicant_user.email)
    end

    it 'should save user_id' do
      repository.save(job_application)
      ja = repository.find(job_application.id)
      expect(ja.user_id).to eq(applicant_user.id)
    end

    it 'should save link_to_cv' do
      repository.save(job_application)
      ja = repository.find(job_application.id)
      expect(ja.link_to_cv).to eq(job_application.link_to_cv)
      expect(ja.job_offer.id).to eq(job_offer.id)
    end

    it 'should save job_offer_id' do
      repository.save(job_application)
      ja = repository.find(job_application.id)
      expect(ja.job_offer.id).to eq(job_offer.id)
    end
  end

  describe 'find_by_user_id' do
    it 'should find application of user' do
      repository.save(job_application)
      job_applications = repository.find_by_user_id(applicant_user.id)
      expect(job_applications.length).to eq(1)
      expect(job_applications[0].user_id).to eq(applicant_user.id)
    end

    it 'should not find application of another user' do
      another_job_application =
        JobApplication.create_for(owner.email, job_offer, 'https://www.linkedin.com/in/roberto-gomez/', owner.id)
      repository.save(another_job_application)
      job_applications = repository.find_by_user_id(applicant_user.id)
      expect(job_applications.length).to eq(0)
    end
  end
end
