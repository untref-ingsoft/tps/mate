class UserRepository < BaseRepository
  self.table_name = :users
  self.model_class = 'User'

  def find_by_email(email)
    row = dataset.first(email: email)
    load_object(row) unless row.nil?
  end

  def find_by_token(token)
    row = dataset.first(password_recovery_token: token)
    load_object(row) unless row.nil?
  end

  protected

  def changeset(user)
    {
      name: user.name,
      crypted_password: user.crypted_password,
      email: user.email,
      password_recovery_token: user.password_recovery_token,
      password_recovery_token_expires_at: user.password_recovery_token_expires_at
    }
  end
end
