class JobOfferTagRepository < BaseRepository
  self.table_name = :job_offer_tag
  self.model_class = 'JobOfferTag'

  def find_by_job_offer_id(job_offer_id)
    load_collection dataset.where(job_offer_id: job_offer_id)
  end

  def delete_by_job_offer_id(job_offer_id)
    job_offer_tags = find_by_job_offer_id(job_offer_id)
    job_offer_tags.each do |job_offer_tag|
      destroy(job_offer_tag)
    end
  end

  def get_related_tags_by_offer_id(job_offer_id)
    job_offer_tags = find_by_job_offer_id(job_offer_id)
    suggestion_offers = []
    job_offer_tags.each do |job_offer_tag|
      offers = load_collection dataset.where(Sequel.lit('(tag = ?) and (job_offer_id != ?)', job_offer_tag.tag,
                                                        job_offer_id))

      suggestion_offers += offers unless offers.empty?
    end
    suggestion_offers
  end

  protected

  def changeset(tag_offer)
    {
      job_offer_id: tag_offer.job_offer_id,
      tag: tag_offer.tag.downcase
    }
  end
end
