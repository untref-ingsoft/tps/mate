class JobOfferApplicationRepository < BaseRepository
  self.table_name = :job_offer_application
  self.model_class = 'JobApplication'

  def find_by_offer_id(job_offer_id)
    load_collection dataset.where(job_offer_id: job_offer_id)
  end

  def find_by_user_id(user_id)
    load_collection dataset.where(user_id: user_id)
  end

  protected

  def load_object(a_record)
    job_offer = JobOfferRepository.new.find(a_record[:job_offer_id])
    JobApplication.new(
      a_record[:applicant_email],
      job_offer,
      a_record[:link_to_cv],
      a_record[:user_id]
    )
  end

  def changeset(job_application)
    {
      job_offer_id: job_application.job_offer.id,
      user_id: job_application.user_id,
      applicant_email: job_application.applicant_email,
      link_to_cv: job_application.link_to_cv
    }
  end
end
