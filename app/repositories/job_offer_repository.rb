class JobOfferRepository < BaseRepository
  self.table_name = :job_offers
  self.model_class = 'JobOffer'

  def all_active
    load_collection dataset.where(is_active: true)
  end

  def find_by_owner(user)
    load_collection dataset.where(user_id: user.id)
  end

  def deactivate_old_offers
    all_active.each do |offer|
      if offer.old_offer?
        offer.deactivate
        update(offer)
      end
    end
  end

  def search_by_title(title)
    load_collection dataset.where(Sequel.like(:title, "%#{title}%"))
  end

  def full_search(pattern)
    result = load_collection dataset
             .left_join(:job_offer_tag, job_offer_id: :id)
             .where(Sequel.ilike(:title, "%#{pattern}%"))
             .or(Sequel.ilike(:description, "%#{pattern}%"))
             .or(Sequel.ilike(:location, "%#{pattern}%"))
             .or(Sequel.ilike(:tag, "%#{pattern}%"))
             .where(is_active: true)
             .select_all(:job_offers)
    result.uniq(&:id)
  end

  def get_suggestions(job_offer_id, size)
    job_offer_suggestions = []
    job_offer_tags = JobOfferTagRepository.new.get_related_tags_by_offer_id(job_offer_id)
    job_offer_tags.each do |job_offer_tag|
      offer = find(job_offer_tag.job_offer_id)
      job_offer_suggestions.push(offer) if offer.is_active
    end
    return job_offer_suggestions.uniq(&:id).first(size) unless job_offer_suggestions.empty?

    job_offer_suggestions
  end

  def increment_applicants(job_offer_id)
    job_offer = find(job_offer_id)
    job_offer.number_of_applicants += 1
    update(job_offer)
  end

  protected

  def load_object(a_record)
    job_offer = super
    job_offer.owner = UserRepository.new.find(job_offer.user_id)
    job_offer.offer_tags = JobOfferTagRepository.new.find_by_job_offer_id(job_offer.id)
    job_offer
  end

  def changeset(offer)
    {
      title: offer.title,
      location: offer.location,
      description: offer.description,
      is_active: offer.is_active,
      user_id: offer.owner&.id || offer.user_id,
      salary: offer.salary,
      number_of_applicants: offer.number_of_applicants || 0
    }
  end
end
