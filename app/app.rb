module JobVacancy
  class App < Padrino::Application
    register Padrino::Rendering
    register Padrino::Mailer
    register Padrino::Helpers

    include Recaptcha::Adapters::ViewMethods
    include Recaptcha::Adapters::ControllerMethods
    Recaptcha.configure do |config|
      config.site_key = ENV['SITE_KEY'] || '6Le5lsQcAAAAACohgTRyjEyiqk--SsUNvIKFR1uY'
      config.secret_key = ENV['SECRET_KEY'] || '6Le5lsQcAAAAAOGz31GRvXyAJotgjIz-QotbtHR7'
    end

    enable :sessions

    Padrino.configure :test do
      set :delivery_method, file: {
        location: "#{Padrino.root}/tmp/emails"
      }
    end

    Padrino.configure :development, :staging, :production do
      set :delivery_method, smtp: {
        address: ENV['SMTP_ADDRESS'] || 'smtp.sendgrid.net',
        port: ENV['SMTP_PORT'] || 587,
        user_name: ENV['SMTP_USER'] || 'apikey',
        password: ENV['SMTP_PASS'] || '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',
        authentication: :plain,
        enable_starttls_auto: true
      }
    end
  end
end
