JobVacancy::App.controllers :job_offers do
  get :my do
    @offers = JobOfferRepository.new.find_by_owner(current_user)
    render 'job_offers/my_offers'
  end

  get :my_applications do
    @job_applications = JobOfferApplicationRepository.new.find_by_user_id(current_user.id)
    render 'job_offers/my_applications'
  end

  get :index do
    @offers = JobOfferRepository.new.all_active
    render 'job_offers/list'
  end

  get :new do
    @job_offer = JobOfferForm.new
    render 'job_offers/new'
  end

  get :latest do
    @offers = JobOfferRepository.new.all_active
    render 'job_offers/list'
  end

  get :edit, with: :offer_id do
    @job_offer = JobOfferForm.from(JobOfferRepository.new.find(params[:offer_id]))
    render 'job_offers/edit'
  end

  get :apply, with: :offer_id do
    if signed_in?
      @job_offer = JobOfferForm.from(JobOfferRepository.new.find(params[:offer_id]))
      @job_offer_suggestions = JobOfferForm.from_array(JobOfferRepository.new.get_suggestions(params[:offer_id], 2))
      @job_application = JobApplicationForm.new
      # TODO: validate the current user is the owner of the offer
      render 'job_offers/apply'
    else
      flash[:error] = 'You must be logged in to apply a job offer'
      redirect '/login'
    end
  end

  post :search do
    @offers = JobOfferRepository.new.full_search(params[:q])
    render 'job_offers/list'
  end

  post :apply, with: :offer_id do
    if verify_recaptcha
      @job_offer = JobOfferRepository.new.find(params[:offer_id])
      applicant_email = params[:job_application_form][:applicant_email]
      link_to_cv = params[:job_application_form][:link_to_cv]
      @job_application = JobApplication.create_for(applicant_email, @job_offer, link_to_cv, current_user.id)
      JobOfferApplicationRepository.new.save(@job_application)
      @job_application.send_contact_email_to_applicant
      @job_application.send_applicant_info_to_offerer
      JobOfferRepository.new.increment_applicants(@job_offer.id)
      flash[:success] = 'Contact information sent.'
      redirect '/job_offers'
    else
      flash[:error] = 'Invalid Captcha'
      redirect "/job_offers/apply/#{params[:offer_id]}"
    end
  rescue ActiveModel::ValidationError => e
    @job_application = JobApplicationForm.new
    @errors = e.model.errors
    flash.now[:error] = 'Please review the errors'
    render 'job_offers/apply'
  end

  post :create do
    job_offer = JobOffer.new(job_offer_params)
    job_offer.owner = current_user
    if JobOfferRepository.new.save(job_offer)
      job_offer.tags.each do |job_offer_tag|
        job_offer_tag.job_offer_id = job_offer.id
        JobOfferTagRepository.new.save(job_offer_tag)
      end
      TwitterClient.publish(job_offer) if params['create_and_twit']
      flash[:success] = 'Offer created'
      redirect '/job_offers/my'
    end
  rescue ActiveModel::ValidationError => e
    @job_offer = JobOfferForm.new
    @errors = e.model.errors
    flash.now[:error] = 'Please review the errors'
    render 'job_offers/new'
  end

  post :update, with: :offer_id do
    @job_offer = JobOffer.new(job_offer_params.merge(id: params[:offer_id]))
    @job_offer.owner = current_user
    if JobOfferRepository.new.save(@job_offer)
      JobOfferTagRepository.new.delete_by_job_offer_id(@job_offer.id)
      @job_offer.tags.each do |job_offer_tag|
        job_offer_tag.job_offer_id = @job_offer.id
        JobOfferTagRepository.new.save(job_offer_tag)
      end
      flash[:success] = 'Offer updated'
      redirect '/job_offers/my'
    end
  rescue ActiveModel::ValidationError => e
    @job_offer = JobOfferForm.new
    @errors = e.model.errors
    flash.now[:error] = 'Please review the errors'
    render 'job_offers/edit'
  end

  put :activate, with: :offer_id do
    @job_offer = JobOfferRepository.new.find(params[:offer_id])
    @job_offer.activate
    if JobOfferRepository.new.save(@job_offer)
      flash[:success] = 'Offer activated'
    else
      flash.now[:error] = 'Operation failed'
    end

    redirect '/job_offers/my'
  end

  delete :destroy do
    applications = JobOfferApplicationRepository.new.find_by_offer_id(params[:offer_id])
    if applications.length.positive?
      flash[:error] = 'You cannot delete an offer with applicants'
    else
      @job_offer = JobOfferRepository.new.find(params[:offer_id])
      if JobOfferRepository.new.destroy(@job_offer)
        JobOfferTagRepository.new.delete_by_job_offer_id(params[:offer_id])
        flash[:success] = 'Offer deleted'
      else
        flash.now[:error] = 'Title is mandatory'
      end
    end
    redirect 'job_offers/my'
  end
end
