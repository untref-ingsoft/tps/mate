INVALID_PW_MSG = 'Invalid password: it must contain an uppercase, a lowercase, a number and 10 characters length'.freeze

JobVacancy::App.controllers :users do
  get :new, map: '/register' do
    @user = User.new
    render 'users/new'
  end

  get :recovery, map: '/password-recovery' do
    @password_recovery = PasswordRecoveryForm.new
    render 'users/password_recovery'
  end

  get :change_password, map: '/password-change' do
    @password_change = PasswordChangeForm.new
    @password_change.token = params[:with]
    render 'users/password_change'
  end

  post :recovery do
    email = params[:password_recovery_form][:email]
    RecoveryPasswordEmailSender.new.send_for(email, request.base_url)
    flash[:success] =
      'If your email address exists in our database,
      you will receive a password recovery link at your email address in a few minutes'
    redirect '/login'
  end

  post :change_password, map: '/password-change' do
    token = params[:password_change_form][:token]
    password = params[:password_change_form][:password]
    password_confirmation = params[:password_change_form][:password_confirmation]
    begin
      ChangePasswordManager.new.change_user_password(token, password, password_confirmation)
      flash[:success] = 'Password recovery successfully'
      redirect '/login'
    rescue ChangePasswordError => e
      flash[:error] = e.message
      redirect "/password-change?with=#{token}"
    end
  end

  post :create do
    password_confirmation = params[:user][:password_confirmation]
    params[:user].reject! { |k, _| k == 'password_confirmation' }

    @user = User.new(params[:user])

    if params[:user][:password] == password_confirmation
      if @user.is_valid_password?
        if UserRepository.new.save(@user)
          flash[:success] = 'User created'
          redirect '/'
        else
          flash.now[:error] = 'All fields are mandatory'
          render 'users/new'
        end
      else
        @user.password = ''
        flash.now[:error] = INVALID_PW_MSG
        render 'users/new'
      end
    else
      flash.now[:error] = 'Passwords do not match'
      render 'users/new'
    end
  end
end
