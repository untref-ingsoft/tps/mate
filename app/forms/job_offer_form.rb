class JobOfferForm
  attr_accessor :id, :title, :location, :description, :salary, :raw_tags

  def self.from(a_job_offer)
    form = JobOfferForm.new
    form.id = a_job_offer.id
    form.title = a_job_offer.title
    form.location = a_job_offer.location
    form.description = a_job_offer.description
    form.salary = a_job_offer.salary
    form.raw_tags = JobOfferTagMapper.new.to_string(a_job_offer.tags)
    form
  end

  def self.from_array(a_job_offers)
    job_offers = []
    a_job_offers.each do |job_offer|
      job_offers.push(from(job_offer))
    end
    job_offers
  end
end
