JobVacancy::App.mailer :password_recovery do
  email :password_recovery_email do |email, restore_url|
    from "Job Vacancy <#{ENV['MAIL_FROM'] || 'srjuancarlostester@gmail.com'}>"
    to email
    subject 'Job Application: Password Recovery'
    locals restore_url: restore_url
    content_type :plain
    render 'password_recovery/password_recovery_email'
  end
end
