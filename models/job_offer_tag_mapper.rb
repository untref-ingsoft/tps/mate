class JobOfferTagMapper
  def from_string(job_offer_id, raw_tags)
    return [] if raw_tags.nil?

    array_of_tags = raw_tags.strip.split(' ').uniq(&:downcase)
    normalized_tags = []
    array_of_tags.each do |current_tag|
      job_offer_tag = JobOfferTag.new(job_offer_id: job_offer_id, tag: current_tag.downcase)
      normalized_tags.push(job_offer_tag)
    end
    normalized_tags
  end

  def to_string(offer_tags)
    return '' if offer_tags.nil?

    raw_tags = ''
    offer_tags.each do |job_offer_tag|
      raw_tags += "#{job_offer_tag.tag} "
    end
    raw_tags.strip
  end
end
