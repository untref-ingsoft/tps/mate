class JobOffer
  include ActiveModel::Validations

  attr_accessor :id, :user, :user_id, :title,
                :location, :description, :salary,
                :tags, :is_active, :updated_on, :created_on,
                :number_of_applicants

  validates :title, presence: true

  validates :salary, allow_nil: true,
                     numericality: { greater_than_or_equal_to: 0,
                                     only_integer: true,
                                     message: 'Remuneration should be a positive number' }

  validates :tags, presence: false,
                   length: { maximum: 5,
                             message: 'The offer cannot have more than five tags' }

  def initialize(data = {})
    @id = data[:id]
    @title = data[:title]
    @location = data[:location]
    @description = data[:description]
    @is_active = data[:is_active]
    @updated_on = data[:updated_on]
    @created_on = data[:created_on]
    @user_id = data[:user_id]
    @salary = data[:salary]&.to_i
    @tags = JobOfferTagMapper.new.from_string(data[:id], data[:raw_tags])
    @number_of_applicants = data[:number_of_applicants]
    validate!
  end

  def owner
    user
  end

  def owner=(a_user)
    self.user = a_user
  end

  def activate
    self.is_active = true
  end

  def deactivate
    self.is_active = false
  end

  def old_offer?
    (Date.today - updated_on) >= 30
  end

  def offer_tags=(tags)
    self.tags = tags
  end
end
