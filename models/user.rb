require 'securerandom'
class User
  include ActiveModel::Validations

  attr_accessor :id, :name, :email, :crypted_password,
                :job_offers, :password, :updated_on, :created_on,
                :password_recovery_token, :password_recovery_token_expires_at

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i.freeze

  validates :name, :crypted_password, presence: true
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX,
                                              message: 'invalid email' }

  def initialize(data = {})
    @id = data[:id]
    @name = data[:name]
    @email = data[:email]
    @crypted_password = if data[:password].nil?
                          data[:crypted_password]
                        else
                          Crypto.encrypt(data[:password])
                        end
    @job_offers = data[:job_offers]
    @updated_on = data[:updated_on]
    @created_on = data[:created_on]
    @password = data[:password]
  end

  def has_password?(password)
    Crypto.decrypt(crypted_password) == password
  end

  def is_valid_password?
    password_validator = PasswordValidator.new(@password)
    password_validator.validate?
  end

  def generate_password_recovery_token
    @password_recovery_token = SecureRandom.hex(32)
    @password_recovery_token_expires_at = Time.now + 1.hour
  end

  def is_password_recovery_token_expired?
    Time.now > @password_recovery_token_expires_at
  end

  def change_password(password)
    @password = password
    @crypted_password = Crypto.encrypt(password)
  end
end
