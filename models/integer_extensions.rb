class IntegerExtensions
  def number_or_zero(salary)
    Integer(salary || '')
  rescue ArgumentError
    0
  end
end
