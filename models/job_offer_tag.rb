class JobOfferTag
  include ActiveModel::Validations

  attr_accessor :id, :job_offer_id, :tag, :updated_on, :created_on

  def initialize(data = {})
    @id = data[:id]
    @job_offer_id = data[:job_offer_id]
    @tag = data[:tag]
    @updated_on = data[:updated_on]
    @created_on = data[:created_on]
  end
end
