class RecoveryPasswordEmailSender
  def send_for(email, base_url)
    current_user = UserRepository.new.find_by_email(email)
    unless current_user.nil?
      current_user.generate_password_recovery_token
      UserRepository.new.save(current_user)
      recovery_url = "#{base_url}/password-change?with=#{current_user.password_recovery_token}"
      JobVacancy::App.deliver(
        :password_recovery,
        :password_recovery_email,
        email,
        recovery_url
      )
    end
  end
end
