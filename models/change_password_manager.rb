class ChangePasswordManager
  INVALID_PW_MSG = 'Invalid password: it must contain an uppercase, a lowercase, a number and 10 characters length'
                   .freeze
  INVALID_PW_MATCH_MSG = 'Passwords do not match'.freeze

  def change_user_password(token, password, password_confirmation)
    user = UserRepository.new.find_by_token(token)
    user.change_password(password)

    validate_password(password, password_confirmation, user)

    UserRepository.new.save(user)
  end

  private

  def validate_password(password, password_confirmation, user)
    raise ChangePasswordError, INVALID_PW_MATCH_MSG unless password == password_confirmation

    raise ChangePasswordError, INVALID_PW_MSG unless user.is_valid_password?
  end
end
