class PasswordValidator
  def initialize(password)
    @password = password
  end

  def validate?
    return false if @password.length != 10

    return false unless @password =~ /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/

    true
  end
end
