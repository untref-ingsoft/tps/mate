require 'uri'

class JobApplication
  include ActiveModel::Validations

  attr_accessor :id, :applicant_email, :job_offer,
                :link_to_cv, :user_id, :updated_on, :created_on

  VALID_URL_REGEX = /\A#{URI::DEFAULT_PARSER.make_regexp}\z/.freeze

  validates :applicant_email, :job_offer, presence: true
  validates :link_to_cv, presence: { message: 'The link to your CV is required' },
                         format: { allow_blank: true,
                                   with: VALID_URL_REGEX,
                                   message: 'The link to your CV is invalid' }
  validates :user_id,
            allow_nil: false,
            numericality: { greater_than_or_equal_to: 0, only_integer: true }

  def initialize(email, offer, link_to_cv, user_id)
    @applicant_email = email
    @job_offer = offer
    @link_to_cv = link_to_cv
    @user_id = user_id
    validate!
  end

  def self.create_for(email, offer, link_to_cv, user_id)
    JobApplication.new(email, offer, link_to_cv, user_id)
  end

  def send_contact_email_to_applicant
    JobVacancy::App.deliver(:notification, :contact_info_email, self)
  end

  def send_applicant_info_to_offerer
    JobVacancy::App.deliver(:notification, :applicant_info_email, self)
  end
end
