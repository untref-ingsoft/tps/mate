Feature: Job Offers CRUD
  In order to get employees
  As a job offerer
  I want to manage my offers

  Background:
  	Given I am logged in as job offerer

  Scenario: Create new offer
    When I create a new offer with "Programmer vacancy" as the title
    Then I should see a offer created confirmation message
    And I should see "Programmer vacancy" in my offers list

  # Escenario: Creacion de oferta con Remuneración con numero positivo  - OK (Ejemplo: 1000)
  Scenario: Create new offer with a valid salary
    When I create a new offer with "Programmer vacancy" as the title and 1000 as the salary
    Then I should see a offer created confirmation message
    And I should see "USD 1000" in my offers list
  
  # Escenario: Creacion de oferta con Remuneración con numero negativo - error: (ejemplo: -5)
  Scenario: Create new offer with an invalid salary
    When I create a new offer with "Programmer vacancy" as the title and -5 as the salary
    Then I should see the message "Remuneration should be a positive number"

  # Escenario: Modificacion de oferta con Remuneración valida - OK: (ejemplo: 2000)
  Scenario: Update offer's salary
    Given I have "Programmer vacancy" offer in my offers list with a salary 1000
    When I change the salary to 2000
    Then I should see a offer updated confirmation message
    And I should see "USD 2000" in my offers list

  Scenario: Update offer
    Given I have "Programmer vacancy" offer in my offers list
    When I change the title to "Programmer vacancy!!!"
    Then I should see a offer updated confirmation message
    And I should see "Programmer vacancy!!!" in my offers list

  Scenario: Delete offer
    Given I have "Programmer vacancy" offer in my offers list
    When I delete it
    Then I should see a offer deleted confirmation message
    And I should not see "Programmer vacancy!!!" in my offers list

  Scenario: Cannot delete offer with applicants
    Given Exists "Programmer vacancy" offer in my offers list
    And "Programmer vacancy" offer has applicants
    When I delete it
    Then I should see the message "You cannot delete an offer with applicants"

  Scenario: Create new offer with one tag
    When I create a new offer with tags "net-developer"
    Then Job offer must have tags "net-developer"

  Scenario: Create new offer with six tags
    When I create a new offer with tags "java c c++ javascript ruby cobol"
    Then I should see the message "The offer cannot have more than five tags"

  Scenario: Create new offer with duplicate tags
    When I create a new offer with tags "JAVA java JaVa Java javA jaVa"
    Then Job offer must have tags "java"
