Feature: Job Application Suggestion
  In order to get a job
  As a candidate
  I want to see suggestion about similar offers when i apply to an offer

  Background: 
    Given exist an offer "Java jr" with tag "java"
    And I am logged in as job offerer

# Escenario: oferta sin sugerencia
  Scenario: Apply to job offer without suggestions
    When I apply to another offer "Ruby Developer" with tag "ruby"
    Then I should not see any suggestion

# Escenario: oferta con una sugerencia
  Scenario: Apply to job offer with one suggestion
    When I apply to another offer "Java Developer" with tag "java"
    Then I should see the suggestion "Java jr"

  Scenario: Apply to job offer with one suggestion
    And exist an offer "ruby sr" with tag "ruby"
    When I apply to another offer "Java Developer" with tag "java"
    Then I should see the suggestion "Java jr"
    And I should not see the suggestion "ruby sr"

  Scenario: Apply to job offer without suggestions because offer is not active
    And create an offer "Java Sr" with tag "java"
    When I apply to another offer "Java Developer" with tag "java"
    Then I should not see the suggestion "Java Sr"