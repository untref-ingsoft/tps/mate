Feature: Job Offers Searching
 
  Scenario: Search by title
    Given exist an offer with title "NET Developer"
    When I search 'net dev'
    Then I should see the offer with title "NET Developer"

  Scenario: Search by description
    Given exist an offer with description "Required: Entity Framework, NET framework and ASP.NET Core"
    When I search 'entity framework'
    Then I should see the offer with description "Required: Entity Framework, NET framework and ASP.NET Core"

  Scenario: Search by location
    Given exist an offer with location "The NET Company"
    When I search 'net company'
    Then I should see the offer with location "The NET Company"

  Scenario: Search by tags
    Given exist an offer with tags "net net-core"
    When I search 'net-core'
    Then I should see the offer with tags "net net-core"

  Scenario: Search without title matching
    Given only exist an offer with title "NET Developer"
    When I search 'ruby'
    Then I should not see any offers

  Scenario: Search without description matching
    Given only exist an offer with description "Required: Entity Framework, NET framework and ASP.NET Core"
    When I search 'ruby'
    Then I should not see any offers

  Scenario: Search without location matching
    Given only exist an offer with location "The NET Company"
    When I search 'ruby'
    Then I should not see any offers

  Scenario: Search without tags matching
    Given only exist an offer with tags "net net-core"
    When I search 'ruby'
    Then I should not see any offers

  Scenario: Case insensitive search
    Given exist an offer with title "nEt DevELoPer"
    When I search 'NET developer'
    Then I should see the offer with title "nEt DevELoPer"