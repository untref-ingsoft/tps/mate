Feature: Recovery password

  Background:
    Given exists an user with email "juangomez@gmail.com"

  Scenario: Password recovery email
    When user request to recover his password with email "juangomez@gmail.com"
    Then user should see the message 'If your email address exists in our database, you will receive a password recovery link at your email address in a few minutes'
    And user should receive an email to change his password

  Scenario: Request password recovery with invalid user
    Given does not exists an user with email "robereto@gmail.com"
    When user request to recover his password with email "robereto@gmail.com"
    Then user should see the message 'If your email address exists in our database, you will receive a password recovery link at your email address in a few minutes'
    And user should not receive an email to change his password

  Scenario: Change password successfully
    Given user set 'u2345M7899' as his new password
    And user set 'u2345M7899' as his confirm password
    When user changes the password
    Then user should see the message 'Password recovery successfully'
    And user can sign in with the password 'u2345M7899'
   
  Scenario: Weak password
    Given user set '12345M7890' as his new password
    And user set '12345M7890' as his confirm password
    When user changes the password
    Then user should see the message 'Invalid password: it must contain an uppercase, a lowercase, a number and 10 characters length'
   
  Scenario: Password does not match
    Given user set 'u2345M7890' as his new password
    And user set '12345M789u' as his confirm password
    When user changes the password
    Then user should see the message 'Passwords do not match'