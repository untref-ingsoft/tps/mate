Feature: My Job Offers

  Background: 
    Given I am logged in as job offerer

  Scenario: Offer with one applicant
    Given I create a new active offer with "Java Developer SR" as the title
    And one user apply to my offer with title "Java Developer SR"
    When I access to my offers list
    Then Job offer must have 1 applicant

  Scenario: Offer without applicant
    Given I create a new active offer with "Programmer vacancy" as the title
    When I access to my offers list
    Then Job offer must have 0 applicant
