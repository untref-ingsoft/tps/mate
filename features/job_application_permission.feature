Feature: Job Application Permission

  Scenario: Cannot apply to job offer without being logged in
    Given I am not logged in
    #And I access to the offers list page
    When I choose "Web Programmer" offer from the offers list page
    Then I should be on "the login page"
    And I should see the message "You must be logged in to apply a job offer"


 