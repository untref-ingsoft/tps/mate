Given('I am not logged in') do
  # Nothing to do
end

When('I choose {string} offer from the offers list page') do |job_title|
  @job_offer = JobOffer.new(title: job_title, location: 'a nice job', description: 'a nice job')
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = true
  JobOfferRepository.new.save @job_offer
  visit '/job_offers'
  click_link 'Apply'
end
