Given('I am not registered') do
  visit '/register'
end

When('I register with name {string} and email {string} and password {string}') do |name, email, password|
  fill_in('user[name]', with: name)
  fill_in('user[email]', with: email)
  fill_in('user[password]', with: password)
  fill_in('user[password_confirmation]', with: password)
  click_button('Create')
end

Then('I should see the message {string}') do |message|
  page.should have_content(message)
end
