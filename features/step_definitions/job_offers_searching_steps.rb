Given('exist an offer with title {string}') do |title|
  @job_offer = JobOffer.new(title: title)
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = true
  JobOfferRepository.new.save @job_offer
end

Given('exist an offer with description {string}') do |description|
  @job_offer = JobOffer.new(title: 'Random Title', description: description)
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = true
  JobOfferRepository.new.save @job_offer
end

Given('exist an offer with location {string}') do |location|
  @job_offer = JobOffer.new(title: 'Random Title', location: location)
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = true
  JobOfferRepository.new.save @job_offer
end

Given('exist an offer with tags {string}') do |tags|
  @job_offer = JobOffer.new(title: 'Random Title', raw_tags: tags)
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = true
  JobOfferRepository.new.save @job_offer
  @job_offer.tags.each do |job_offer_tag|
    job_offer_tag.job_offer_id = @job_offer.id
    JobOfferTagRepository.new.save(job_offer_tag)
  end
end

Given('only exist an offer with title {string}') do |title|
  @job_offer = JobOffer.new(title: title)
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = true
  JobOfferRepository.new.save @job_offer
end

Given('only exist an offer with description {string}') do |description|
  @job_offer = JobOffer.new(title: 'Random Title', description: description)
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = true
  JobOfferRepository.new.save @job_offer
end

Given('only exist an offer with location {string}') do |location|
  @job_offer = JobOffer.new(title: 'Random Title', location: location)
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = true
  JobOfferRepository.new.save @job_offer
end

Given('only exist an offer with tags {string}') do |tags|
  @job_offer = JobOffer.new(title: 'Random Title', raw_tags: tags)
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = true
  JobOfferRepository.new.save @job_offer
  @job_offer.tags.each do |job_offer_tag|
    job_offer_tag.job_offer_id = @job_offer.id
    JobOfferTagRepository.new.save(job_offer_tag)
  end
end

When('I search {string}') do |search_pattern|
  visit '/job_offers/latest'
  fill_in('q', with: search_pattern)
  click_button('search')
end

Then('I should see the offer with title {string}') do |title|
  page.should have_content(title)
end

Then('I should see the offer with description {string}') do |description|
  page.should have_content(description)
end

Then('I should see the offer with location {string}') do |location|
  page.should have_content(location)
end

Then('I should see the offer with tags {string}') do |tags|
  page.should have_content(tags)
end

Then('I should not see any offers') do
  expect(find_by_id('currentJobOffers').has_no_content?).to be true
end
