Given('exists an user with email {string}') do |email|
  @user = User.new(name: 'Joe', email: email, crypted_password: 'Asdasd1234')
  @user.generate_password_recovery_token
  UserRepository.new.save(@user)
end

When('user request to recover his password with email {string}') do |email|
  @change_password_email = email
  visit '/password-recovery'
  fill_in('password_recovery_form[email]', with: email)
  click_button('Recovery')
end

Then('user should see the message {string}') do |message|
  page.should have_content(message)
end

Then('user should receive an email to change his password') do
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/#{@user.email}", 'r')
  content = file.read
  content.include?("We've recieved a request to reset your password").should be true
end

Given('does not exists an user with email {string}') do |_string|
  # Do nothing
end

Then('user should not receive an email to change his password') do
  mail_store = "#{Padrino.root}/tmp/emails"
  user_exists = File.exist?("#{mail_store}/#{@change_password_email}")
  expect(user_exists).to eq(false)
end

Given('user set {string} as his new password') do |password|
  visit "/password-change?with=#{@user.password_recovery_token}"
  fill_in('password_change_form[password]', with: password)
end

Given('user set {string} as his confirm password') do |password_confirmation|
  fill_in('password_change_form[password_confirmation]', with: password_confirmation)
end

When('user changes the password') do
  click_button('Change')
end

Then('user can sign in with the password {string}') do |password|
  fill_in('user[email]', with: @user.email)
  fill_in('user[password]', with: password)
  click_button('Login')
  page.should have_content(@user.email)
end
