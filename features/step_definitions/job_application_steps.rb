Given('only a {string} offer exists in the offers list') do |job_title|
  @job_offer = JobOffer.new(title: job_title, location: 'a nice job', description: 'a nice job')
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = true
  JobOfferRepository.new.save @job_offer
end

Given('I access to the offers list page') do
  visit '/job_offers'
end

Given('I choose') do
  click_link 'Apply'
end

Given('my email is {string}') do |applicant_email|
  fill_in('job_application_form[applicant_email]', with: applicant_email)
end

Given('the link to my CV is {string}') do |link_to_cv|
  fill_in('job_application_form[link_to_cv]', with: link_to_cv)
end

Given('I solve the captcha') do
  Recaptcha.configuration.skip_verify_env.push('test')
end

Given('I do not solve the captcha') do
  Recaptcha.configuration.skip_verify_env.delete('test')
end

When('I apply') do
  click_button('Apply')
end

Then('I should receive a mail with offerer info') do
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/applicant@test.com", 'r')
  content = file.read
  content.include?(@job_offer.title).should be true
  content.include?(@job_offer.location).should be true
  content.include?(@job_offer.description).should be true
  content.include?(@job_offer.owner.email).should be true
  content.include?(@job_offer.owner.name).should be true
end

Given('I apply to {string} offer') do |_title|
  fill_in('job_application_form[applicant_email]', with: 'test123321@gmail.com')
  fill_in('job_application_form[link_to_cv]', with: 'https://www.linkedin.com/')
  Recaptcha.configuration.skip_verify_env.push('test')
  click_button('Apply')
end

When('I access to my applications') do
  visit '/job_offers/my_applications'
end
