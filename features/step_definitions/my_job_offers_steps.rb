Given('I create a new active offer with {string} as the title') do |job_title|
  job_offer = JobOffer.new(title: job_title, location: 'a nice job', description: 'a nice job')
  job_offer.owner = UserRepository.new.first
  job_offer.is_active = true
  JobOfferRepository.new.save job_offer
end

Given('one user apply to my offer with title {string}') do |_title|
  visit '/job_offers'
  click_link 'Apply'
  fill_in('job_application_form[applicant_email]', with: 'test123321@gmail.com')
  fill_in('job_application_form[link_to_cv]', with: 'https://www.linkedin.com/')
  Recaptcha.configuration.skip_verify_env.push('test')
  click_button('Apply')
end

When('I access to my offers list') do
  visit '/job_offers/my'
end

Then('Job offer must have {int} applicant') do |applicants|
  page.should have_content(applicants)
end
