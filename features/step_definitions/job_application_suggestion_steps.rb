Given('exist an offer {string} with tag {string}') do |title, tag|
  @job_offer = JobOffer.new(title: title, raw_tags: tag)
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = true
  JobOfferRepository.new.save @job_offer
  @job_offer.tags.each do |job_offer_tag|
    job_offer_tag.job_offer_id = @job_offer.id
    JobOfferTagRepository.new.save(job_offer_tag)
  end
end

Given('create an offer {string} with tag {string}') do |title, tag|
  @job_offer = JobOffer.new(title: title, raw_tags: tag)
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = false
  JobOfferRepository.new.save @job_offer
  @job_offer.tags.each do |job_offer_tag|
    job_offer_tag.job_offer_id = @job_offer.id
    JobOfferTagRepository.new.save(job_offer_tag)
  end
end

When('I apply to another offer {string} with tag {string}') do |title, tag|
  @job_offer = JobOffer.new(title: title, raw_tags: tag)
  @job_offer.owner = UserRepository.new.first
  @job_offer.is_active = true
  JobOfferRepository.new.save @job_offer
  @job_offer.tags.each do |job_offer_tag|
    job_offer_tag.job_offer_id = @job_offer.id
    JobOfferTagRepository.new.save(job_offer_tag)
  end
  visit "/job_offers/apply/#{@job_offer.id}"
end

Then('I should not see any suggestion') do
  page.should have_content('No related offers found')
end

Then('I should see the suggestion {string}') do |title|
  page.should have_content(title)
end

Then('I should not see the suggestion {string}') do |title|
  page.should have_no_content(title)
end
