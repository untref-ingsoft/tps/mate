Feature: Registration

  Background:
    Given I am not registered
    
  # Escenario: Contraseña con 10 caracteres - OK (Ejemplo: u2345M7890)
  Scenario: Strong password
    When I register with name 'Juan Perez' and email 'juanperez@gmail.com' and password 'u2345M7890'
    Then I should see the message 'User created'

  # Escenario: Contraseña con 9 caracteres - error: (Ejemplo: u2345M789)
  Scenario: Invalid password with 9 characters
    When I register with name 'Juan Perez' and email 'juanperez@gmail.com' and password 'u2345M789'
    Then I should see the message 'Invalid password: it must contain an uppercase, a lowercase, a number and 10 characters length'
  
  # Escenario: Contraseña con 11 caracteres - error: (Ejemplo: u2345M78900)
  Scenario: Invalid password with 11 characters
    When I register with name 'Juan Perez' and email 'juanperez@gmail.com' and password 'u2345M78900'
    Then I should see the message 'Invalid password: it must contain an uppercase, a lowercase, a number and 10 characters length'

  # Escenario: Contraseña sin mayúsculas - error (Ejemplo: u234567890)
  Scenario: Invalid password without an uppercase letter
    When I register with name 'Juan Perez' and email 'juanperez@gmail.com' and password 'u234567890'
    Then I should see the message 'Invalid password: it must contain an uppercase, a lowercase, a number and 10 characters length'

  # Escenario: Contraseña sin minúsculas - error (Ejemplo: T234567890)
  Scenario: Invalid password without a lowercase letter
    When I register with name 'Juan Perez' and email 'juanperez@gmail.com' and password 'T234567890'
    Then I should see the message 'Invalid password: it must contain an uppercase, a lowercase, a number and 10 characters length'

  # Escenario: Contraseña sin números - error (Ejemplo: AbCDEfGhiZ)
  Scenario: Invalid password without a digit
    When I register with name 'Juan Perez' and email 'juanperez@gmail.com' and password 'AbCDEfGhiZ'
    Then I should see the message 'Invalid password: it must contain an uppercase, a lowercase, a number and 10 characters length'
