Feature: Job Application
  In order to get a job
  As a candidate
  I want to apply to an offer

  Background:
    Given I am logged in as job offerer
    And only a "Web Programmer" offer exists in the offers list 
  	And I access to the offers list page
    And I choose

  Scenario: Apply to job offer
    Given my email is "applicant@test.com" 
    And the link to my CV is "https://www.linkedin.com/in/juan-gomez/" 
    And I solve the captcha
    When I apply 
    Then I should receive a mail with offerer info

  Scenario: Apply to job offer without CV
    Given my email is "applicant@test.com" 
    And I solve the captcha
    When I apply 
    Then I should see the message "The link to your CV is required"

  Scenario: Apply to job offer with invalid link to my CV
    Given my email is "applicant@test.com" 
    And the link to my CV is "https://www.linkedin.com @ abc" 
    And I solve the captcha
    When I apply 
    Then I should see the message "The link to your CV is invalid"

  Scenario: Apply to job offer without solving captcha
    Given my email is "applicant@test.com" 
    And the link to my CV is "https://www.linkedin.com/in/juan-gomez/"
    And I do not solve the captcha
    When I apply 
    Then I should see the message "Invalid Captcha"

  Scenario: Apply to job offer and See that application in my applications page
    Given I apply to "Web Programmer" offer
    When I access to my applications
    Then I should see the offer with title "Web Programmer"
