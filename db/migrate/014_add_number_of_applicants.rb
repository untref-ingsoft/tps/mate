Sequel.migration do
  up do
    add_column :job_offers, :number_of_applicants, Integer, default: 0, null: false
  end

  down do
    drop_column :job_offers, :number_of_applicants
  end
end
