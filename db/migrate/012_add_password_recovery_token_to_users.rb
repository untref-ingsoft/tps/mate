Sequel.migration do
  up do
    add_column :users, :password_recovery_token, String
    add_column :users, :password_recovery_token_expires_at, Date
  end

  down do
    drop_column :users, :password_recovery_token
    drop_column :users, :password_recovery_token_expires_at
  end
end
