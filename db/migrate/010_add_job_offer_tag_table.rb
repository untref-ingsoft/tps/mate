Sequel.migration do
  up do
    create_table(:job_offer_tag) do
      primary_key :id
      Integer :job_offer_id
      String :tag
    end
  end

  down do
    drop_table(:job_offer_tag)
  end
end
