Sequel.migration do
  up do
    create_table(:job_offer_application) do
      primary_key :id
      Integer :job_offer_id
      Integer :user_id
      String  :link_to_cv
      String  :applicant_email
      Date    :created_on
      Date    :updated_on
    end
  end

  down do
    drop_table(:job_offer_application)
  end
end
