Sequel.migration do
  up do
    add_column :job_offer_tag, :created_on, Date
    add_column :job_offer_tag, :updated_on, Date
  end

  down do
    drop_column :job_offer_tag, :created_on
    drop_column :job_offer_tag, :updated_on
  end
end
